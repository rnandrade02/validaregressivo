from behave import given, when, then
import requests

@given('api endpoint "{url}"')
def step_given_api_endpoint(context, url):
    context.url = url

@when('faço uma requisição GET para a api')
def step_get_request(context):
    context.response = requests.get(context.url)

@then('status code da resposta deve ser {status_code}')
def step_check_status_code(context, status_code):
    print(status_code)