# app.py
from flask import Flask, render_template, request
import subprocess

app = Flask(__name__)

@app.route('/')
def home():
    return render_template('index.html')

@app.route('/run_test', methods=['GET'])
def run_test():
    try:
        subprocess.run(["behave"], check=True)
        return 'Teste executado com sucesso!'
    except Exception as e:
        app.logger.error(f"Erro ao executar teste: {e}")
        return 'Erro ao executar teste', 500



if __name__ == '__main__':
    app.run(debug=True)
